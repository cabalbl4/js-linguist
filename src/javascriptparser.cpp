#include "javascriptparser.h"

#include <QByteArray>
#include <QThread>
#include <QDebug>
#include <QApplication>

namespace parsers {


#define MUSTRESET \
tSequence = false;\
rSequence = false;\
preSequence = false;\
bOpening = false;\
openingCharAvail = false;\
openingChar = QChar();\
interruptCharAvail = false;\
interruptChar = QChar();\
    interruptBuffer = "";\
closed = false;\
    afterSpace = false ;\
    trFromDigit ="";\
    resultData="";\
    commentSkipOne = false;\
    commentMode = false;\


QMap<QString, QList<int> > JavascriptParser::parseJSString(const QString &data_s)
{
    QMap<QString,QList<int>> l_result;

    bool preSequence = false;
    bool tSequence = false;
    bool rSequence = false;

    // ( ...
    bool bOpening = false;
    // ( -> "/'...
    bool openingCharAvail = false;
    QChar openingChar;

    bool interruptCharAvail = false;
    QChar interruptChar;
    QString interruptBuffer;

    bool closed = false;
    bool afterSpace = false;
    QString trFromDigit;
    QString resultData;

    bool commentSkipOne = false;
    bool commentMode = false;


// TODO Parse inline comments
    for(int i=0; i<data_s.length(); i++) {
   // if(i%100 ==0)  qDebug()<<"parse symbol:"<<i;




        if(commentSkipOne) {
          commentSkipOne = false;
          continue;
        };

        if(commentMode) {
             if(data_s.at(i) == '*') {
               if(i < data_s.length() -1) {
                 if(data_s.at(i+1) == '/') {
                   commentSkipOne = true;
                   commentMode = false;
                   continue;
                 };
               };
             };
        }

        if(! ((openingCharAvail) &&(! closed)) ) {
            if(data_s.at(i) == '/') {
                if(i < data_s.length() -1) {
                  if(data_s.at(i+1) == '*') {
                    commentSkipOne = true;
                    commentMode = true;
                    continue;
                  };
                };
            };

        };

       if(afterSpace) {
         if(data_s.at(i).isSpace() || data_s.at(i) == '\n' ||  data_s.at(i) == ')') {
             if(trFromDigit.length() > 0) {
                int form = trFromDigit.toInt();
                if(form == 0) form = 1;
                QList<int> tlist = l_result[resultData];
                if(tlist.count(form) == 0) tlist.append(form);
                l_result[resultData] = tlist;
                MUSTRESET
                continue;
             } else {
                 if(data_s.at(i) == ')') {
                      MUSTRESET
                      continue;
                 }
                continue;
             }
         }
         if(data_s.at(i).isDigit() ) {
           trFromDigit += data_s.at(i);
           continue;
         };

         MUSTRESET
         continue;

       }; // afterspace

       if(closed) {
           if(data_s.at(i).isSpace()) continue;
           if(data_s.at(i) == '\n') continue;
           if(data_s.at(i) == ')') {
               QList<int> tlist = l_result[resultData];
               if(tlist.count(1) == 0) tlist.append(1);
               l_result[resultData] = tlist;
               MUSTRESET
               continue;
           }
           if(data_s.at(i) == ',') {
               afterSpace = true;
               continue;
           }
           MUSTRESET
           continue;

       };//closed

       if(interruptCharAvail) {
         if(interruptChar == '\\') {
             if(data_s.at(i).isSpace()) {
               interruptBuffer += ' ';
               continue;
             };
             if(data_s.at(i) == '\n') {
               interruptCharAvail = false;
               interruptChar = QChar();
               continue;
             };

             if(data_s.at(i) == openingChar) {
                 if(interruptBuffer.length() <= 1) {
                     interruptCharAvail = false;
                     interruptChar = QChar();
                     resultData += openingChar;
                     continue;
                 } else {
                     resultData += interruptBuffer;
                     interruptBuffer = "";
                     closed = true;
                     continue;
                 }
             };


             resultData += interruptBuffer;
             interruptBuffer = "";
             continue;

         }




       }// interruptCharAvail;


       if(openingCharAvail) {
         if(data_s.at(i) == '\\') {
           interruptBuffer = '\\';
           interruptChar = '\\';
           interruptCharAvail = true;
           continue;
         };

         if(data_s.at(i) == '\n') {
             MUSTRESET
                     continue;
         }

         if(data_s.at(i) == openingChar) {
           closed = true;
           continue;
         };

         resultData += data_s.at(i);
         continue;

       }; // openingChar


       if(bOpening) {
         if(data_s.at(i) == '\n') {
             continue;
         }
         if(data_s.at(i) == '\t') {
             continue;
         }
         if(data_s.at(i).isSpace()) continue;
         if((data_s.at(i) == '\'' ) || (data_s.at(i) == '\"')) {
             openingChar = data_s.at(i);
                 openingCharAvail = true;
                 continue;
         };
           MUSTRESET
           continue;

       };

       if(rSequence) {
           if(data_s.at(i) == '\n') {
               continue;
           }
           if(data_s.at(i) == '\t') {
               continue;
           }
           if(data_s.at(i).isSpace()) continue;
           if(data_s.at(i) == '(') {
             bOpening = true;
             continue;
           };
           MUSTRESET
           continue;
       };

       if(tSequence) {
           if(data_s.at(i) == 'r') {
               rSequence = true;
               continue;
           }
           MUSTRESET
           continue;
       };

       if(preSequence || (i==0)) {
          if(data_s.at(i) == 't') {
              tSequence = true;
              continue;
          };
          if((data_s.at(i) == '(' )||
          (data_s.at(i) == '=')||
          (data_s.at(i) == '+') || 
          (data_s.at(i) == ';') || 
          (data_s.at(i) == '\t') || 
          (data_s.at(i) == '\n') ||  
          (data_s.at(i).isSpace())) continue;
          MUSTRESET
          continue;
       };

       if((data_s.at(i) == '(' )||
       (data_s.at(i) == '=')||
       (data_s.at(i) == '+')||
       (data_s.at(i) == ';') || 
       (data_s.at(i) == '\t') || 
       (data_s.at(i) == '\n') ||  
       (data_s.at(i).isSpace()))
       preSequence = true;




    };//for
    return l_result;
}

JavascriptParser::JavascriptParser(const QString& _javascriptFile) : AbstractParser(),
    mFile(_javascriptFile)
{
mEnd = false;
}


void JavascriptParser::startParsing()
{
    mSemaphore->acquire();
  //  qDebug() << "Javascript parser start";
    emit progress(this, 0);
      QStringList fatals;
    if(! mFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
      QString err = tr("Can not open file:")+mFile.fileName();
      fatals.append(err);
      emit(fatalErrors(this, fatals));
      this->deleteLater();
      mSemaphore->release();
      return;
    };

    QByteArray data = mFile.readAll();
    QString data_s(data);
    mFile.close();

    QMap<QString,QList<int>> l_result;


    emit progress(this, 50);

    l_result = parseJSString(data_s);

    auto string_data =  parseNestedStrings(data_s);

    foreach(QString key, string_data.keys()) {
        if(l_result.contains(key)) {
           QList<int> what = l_result[key];
           foreach(int val,string_data[key]) {
               if(what.count(val)==0) what.append(val);
           }
           l_result[key]=what;
        } else {
           l_result[key] = string_data[key];
        }
    }
mSemaphore->release();
    if(mEnd == true) return;
    //qDebug() << "Javascript parser all good";

    emit progress(this, 100);
    emit(result(this, l_result));
    this->deleteLater();



}

void JavascriptParser::abort()
{
    mEnd = true;
    this->deleteLater();
    blockSignals(this);
}

QMap<QString, QList<int> > JavascriptParser::parseNestedStrings(const QString& str)
{
    //QRegExp rx("('|\")(<|.+<)[a-z]+.+( tr=| trform=)('|\"|\\'|\\\").+('|\"|\\'|\\\")(>|.+>)");

    QStringList list = str.split(" tr=");


    QMap<QString, QList<int> > l_result;
    if(list.length() == 1) return l_result;

    bool skip = true;
    foreach(QString s, list) {
      if(skip) {
      skip = false;
      continue;
      };
        QString trStr = parseTR(" tr="+s);
      if(! trStr.isEmpty()) {
        QList<int> forms = l_result[trStr];
        int form = parseTRForm(s);
        if(forms.indexOf(form) == -1) forms.append(form);
        l_result[trStr] = forms;


      };

    };


    return l_result;

}

QString JavascriptParser::parseTR(QString script)
{
    if(script.indexOf("tr") == -1) return QString();
    script.remove(0, script.indexOf("tr")+4);
    QString ident;
    if(script.length() < 3) return QString();
    if(script.at(0) == '\\' ) {
        ident = "\\";
         ident += script.at(1);
    } else {
        ident = script.at(0);
    }
    if((ident != "\"") && (ident!="'") && (ident!="\\\"") && (ident!="\\'"))
        return QString();

   QStringList all = script.split(ident);
   if(all.length() == 0) return QString();
   return all.at(0);
}

int JavascriptParser::parseTRForm(QString script)
{
    if(script.indexOf("trform") == -1) return 1;
    script.remove(0, script.indexOf("trform")+8);
    QString ident;
    if(script.length() < 3) return 1;
    if(script.at(0) == '\\' ) {
        ident = "\\";
         ident += script.at(1);
    } else {
        ident = script.at(0);
    }
    if((ident != "\"") && (ident!="'") && (ident!="\\\"") && (ident!="\\'"))
        return 1;

   QStringList all = script.split(ident);
   if(all.length() == 0) return 1;
    bool ok =true;
    int test = all.at(0).toInt(&ok);
    if(ok) return test;
    return 1;
}

}

