#include <iostream>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QDebug>
#include <QFileDialog>
#include <QDirIterator>
#include <QMessageBox>
#include <QInputDialog>
#include <QListWidget>
#include <QListWidgetItem>
#include <QDesktopServices>

#include "mainwindow.h"
#include "ui_mainwindow.h"


#include "property.h"
#include "htmltrimbunerdialog.h"
#include "langjsfile.h"



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mCurrent(nullptr)
{
    ui->setupUi(this);
    mProgressDialog = nullptr;
    mCurrentlySelected.setSetter(this, &MainWindow::selectionChangeTrigger);

   //Create ISO language helpers
    try {
        QFile langFile(":/data/languages.json");
        if(! langFile.open(QIODevice::ReadOnly | QIODevice::Text) ) throw std::runtime_error( "No open" );


        QByteArray langJsonRaw = langFile.readAll();
        langFile.close();
        QJsonParseError e;
        QJsonDocument langData = QJsonDocument::fromJson(langJsonRaw, &e);
        qDebug() << e.errorString();
        QJsonObject langs = langData.object();
        ui->comboBoxShortName->addItems(langs.keys());
        QStringList fullNames;
        foreach (QString key, langs.keys()) {
           fullNames.append( langs.value(key).toArray().at(1).toString(key) );
        }
        ui->comboBoxFullName->addItems(fullNames);
    } catch (...) {
        std::cerr << "Error parsing lang file languages.json";
        exit(10);
   }

//    QFile f("/home/hagel/WORK/js_linguist/js_tr_ru.js");
//    LangJsFile fl = LangJsFile::fromJsFile(f);
//    std::cout<<"fullname" << fl.langFullName.get().toStdString();
    refreshView();
}

void MainWindow::MainWindow::closeEvent(QCloseEvent *event)
{
    if((mCurrent == nullptr) || (mCurrent->isSaved())){
           event->accept();
           return;
    }

    QMessageBox msgBox;
    msgBox.setText(tr("The document has been modified."));
    msgBox.setInformativeText(tr("Do you want to save your changes?"));
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    int ret = msgBox.exec();

    switch (ret) {
      case QMessageBox::Save:
        // Save was clicked
       if(mCurrent->lastFile.get().isEmpty()) {
           QString file = QFileDialog::getSaveFileName(this,
                                                           tr("Select saving name"),
                                                           tr(""),
                                                           tr("Language files(*.js)"));
           if(file.isEmpty() ) {
            event->accept();
            return;
           };
           mCurrent->lastFile = file;

       }

        if(! mCurrent->save() ) {
               QMessageBox msgBox;
               msgBox.setWindowTitle(tr("Warning"));
               msgBox.setIcon(QMessageBox::Warning);
               msgBox.setText(tr("Can not save this file. Operation abored."));
               msgBox.exec();
               event->ignore();
               return;
           };

          event->accept();

          return;
          break;
      case QMessageBox::Discard:
        event->accept();
          return;
          break;
      case QMessageBox::Cancel:
         event->ignore();
        return;
          break;
      default:
          // should never be reached
        return;
          break;
    }
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_comboBoxFullName_currentIndexChanged(int index)
{
    ui->comboBoxShortName->setCurrentIndex(index);
    if(mCurrent != nullptr) {
      mCurrent->langFullName = ui->comboBoxFullName->currentText();
      mCurrent->langShortName = ui->comboBoxShortName->currentText();
    };
}

void MainWindow::on_comboBoxShortName_currentIndexChanged(int index)
{
    ui->comboBoxFullName->setCurrentIndex(index);
    if(mCurrent != nullptr) {
      mCurrent->langFullName = ui->comboBoxFullName->currentText();
      mCurrent->langShortName = ui->comboBoxShortName->currentText();
    };
}

void MainWindow::on_actionAdd_from_dir_triggered()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Project Directory"),
                                                "",
                                                QFileDialog::ShowDirsOnly
                                                | QFileDialog::DontResolveSymlinks);
    if(dir.isEmpty()) return;
    QStringList allFiles;
    QDirIterator it(dir, QStringList() << "*.*", QDir::Files, QDirIterator::Subdirectories);
    while (it.hasNext()) {
      allFiles.append(it.next());
    };

    generateAddition(allFiles, false);


}


void MainWindow::on_actionAdd_from_file_triggered()
{
    QStringList allFiles = QFileDialog::getOpenFileNames(this,
                                                tr("Open file to import data"),
                                                tr(""),
                                                tr("Parseable(*.html *.htm *.js)"));



     generateAddition(allFiles, false);
}

void MainWindow::generateAddition(const QStringList &allFiles, bool syncMode)
{
    QMap<QString, parsers::ParserDispatcher::PARSER_TYPE> types;
    types["HTML"] = parsers::ParserDispatcher::PARSER_HTML;
    types["HTM"] = parsers::ParserDispatcher::PARSER_HTML;
    types["JS"] = parsers::ParserDispatcher::PARSER_JAVASCRIPT;

    parsers::ParserDispatcher* p = new parsers::ParserDispatcher(allFiles,types);


    mProgressDialog = new ModalProgressMeter(tr("Parsing dir"), this);
    mProgressDialog->show();

    connect(p,SIGNAL(overallProgress(int)), mProgressDialog, SLOT(progress(int)) );
    connect(p,SIGNAL(overallErrors(QStringList)), mProgressDialog, SLOT(recieveErrStrings(QStringList)));

    if(syncMode) {
        QObject::connect(p,
                         &parsers::ParserDispatcher::overallResult,
                this,
                         &MainWindow::parserSyncResult);

    } else {
    QObject::connect(p,
                     &parsers::ParserDispatcher::overallResult,
            this,
                     &MainWindow::parserAddResult);
    }

    p->startParsing();
}

void MainWindow::makeItemsEditable()
{
    for(int i=0; i<ui->optonsListWidget->count(); i++ )
        ui->optonsListWidget->item(i)->setFlags( ui->optonsListWidget->item(i)->flags() |  Qt::ItemIsEditable );
}

void MainWindow::selectionChangeTrigger(sproperty &param, const QString &what)
{
    QString l_what = what;
    if(! mCurrent) {
        l_what = QString();
    } else {
        if(! mCurrent->definitions.get().contains(what))  l_what = QString();
    }



    if(l_what.isEmpty()) {
      ui->addTranslationPushButton->setDisabled(true);
      ui->removeSelectedTranslation->setDisabled(true);
      ui->saveValuesPushButton->setDisabled(true);
      ui->optonsListWidget->setDisabled(true);
      ui->optonsListWidget->clear();
    } else {
          if(*param.valuePtr() == l_what) return;
        ui->addTranslationPushButton->setDisabled(false);
        ui->removeSelectedTranslation->setDisabled(false);
        ui->saveValuesPushButton->setDisabled(false);
        ui->optonsListWidget->setDisabled(false);
        ui->optonsListWidget->clear();
        ui->optonsListWidget->addItems( mCurrent->definitions.get()[what]);
        makeItemsEditable();



    };

    *param.valuePtr() = l_what;

}



void MainWindow::refreshView()
{

    ui->allKeysListWidget->clear();

    if(! mCurrent) {
        ui->allKeysListWidget->setDisabled(true);
        ui->addTranslationKeyPushButton->setDisabled(true);
        ui->removeSelectedPushButton->setDisabled(true);
        ui->comboBoxFullName->setDisabled(true);
        ui->comboBoxShortName->setDisabled(true);
        ui->renameSelectedPushButton->setDisabled(true);
        ui->actionSave->setDisabled(true);
        ui->actionSave_as->setDisabled(true);
        ui->actionClose->setDisabled(true);
        ui->findLineEdit->setDisabled(true);
        mCurrentlySelected = QString();
        return;
    };


    ui->actionSave->setDisabled(false);
    ui->actionSave_as->setDisabled(false);
    ui->actionClose->setDisabled(false);
    ui->findLineEdit->setDisabled(false);

    ui->allKeysListWidget->setDisabled(false);
    ui->addTranslationKeyPushButton->setDisabled(false);
    ui->removeSelectedPushButton->setDisabled(false);
    ui->renameSelectedPushButton->setDisabled(false);
    ui->comboBoxFullName->setDisabled(false);
    ui->comboBoxShortName->setDisabled(false);

    ui->comboBoxFullName->blockSignals(true);
    ui->comboBoxShortName->blockSignals(true);
    ui->comboBoxFullName->setCurrentText( mCurrent->langFullName.get() );
    ui->comboBoxShortName->setCurrentText( mCurrent->langShortName.get() );
    ui->comboBoxFullName->blockSignals(false);
    ui->comboBoxShortName->blockSignals(false);

    QString finder = ui->findLineEdit->text();
    foreach(QString key, mCurrent->definitions.get().keys()) {

        //Filter
        if(!finder.isEmpty()) {
          if(key.indexOf(finder) == -1) continue;
        };


        ui->allKeysListWidget->addItem(key);

        if(mCurrentlySelected == key) ui->allKeysListWidget->setCurrentItem(
                    ui->allKeysListWidget->item(ui->allKeysListWidget->count() - 1)

                    );

        //Show in color inconsistent
        if(mNonExistables.indexOf(key) != -1) {
            ui->allKeysListWidget->item(ui->allKeysListWidget->count() - 1)->setBackgroundColor( QColor("red") );
        };
    };
    ui->allKeysListWidget->sortItems();


}

void MainWindow::parserAddResult(QMap<QString, QList<int> > data)
{
    if(mProgressDialog != nullptr) mProgressDialog->annihilate();
    mProgressDialog = nullptr;
    qDebug()<<data;
    if(! mCurrent) {
        if(! safeCreateNewTrFile()) return;
    }
    QMap<QString, QStringList> map = mCurrent->definitions.get();
    foreach (QString key, data.keys()) {
      QList<QString> forms = map[key];
      QList<int> intForms = data[key];
      int maxForm = 1;
      for(int i=0; i<intForms.length(); i++) {
//          while(intForms.at(i) >= forms.length()) {
//            forms.append(key);
//          };
          if(intForms.at(i) > maxForm) maxForm = intForms.at(i);
      }
      for(int i=0; i<maxForm; i++) {
          forms.append(key);
      }

      map[key] = forms;
    };
    mCurrent->definitions = map;
    refreshView();
}

void MainWindow::parserSyncResult(QMap<QString, QList<int> > data)
{
    if(mProgressDialog != nullptr) mProgressDialog->annihilate();
    mProgressDialog = nullptr;
    qDebug()<<data;
    if(! mCurrent) {
        if(! safeCreateNewTrFile()) return;
    }

    QStringList nonExistables;

    QMap<QString, QStringList> map = mCurrent->definitions.get();
    foreach (QString key, mCurrent->definitions.get().keys()) {
        if(data.keys().indexOf(key) == -1) {
          nonExistables.append(key);
        };
    }


    foreach (QString key, data.keys()) {
    //   if(! mCurrent->definitions.get().contains(key)) nonExistables.append(key);
      QList<QString> forms = map[key];
      QList<int> intForms = data[key];
      for(int i=0; i<intForms.length(); i++) {
          while(intForms.at(i) >= forms.length()) {
            forms.append(key);
          };
      }
      map[key] = forms;
    };




    mNonExistables = nonExistables;
    mCurrent->definitions = map;
    refreshView();
}

void MainWindow::on_addTranslationKeyPushButton_clicked()
{
    if(mCurrent == nullptr) {
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Warning"));
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText(tr("Please open a file or create a new one."));
        msgBox.exec();
      return;
    };


    bool ok;
        QString text = QInputDialog::getText(this, tr("Please input new key name"),
                                             tr("Tr key name:"), QLineEdit::Normal,
                                             tr("No name"), &ok);
        if (ok && !text.isEmpty()) {

            if(mCurrent->definitions.get().keys().contains(text)) {
                QMessageBox msgBox;
                msgBox.setWindowTitle(tr("Warning"));
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.setText(tr("This key already exists."));
                msgBox.exec();
                return;
            };
        auto defs = mCurrent->definitions.get();
        QList<QString> a;
        a << text;
        defs[text] = a;
        mCurrent->definitions = defs;

        };

    refreshView();

}

bool MainWindow::safeCreateNewTrFile()
{
    if(mCurrent == nullptr) {
      mCurrent = new LangJsFile();
      mNonExistables.clear();
      refreshView();
      return true;
    };

    if(mCurrent->isSaved()) {
        delete mCurrent;
        mCurrent = new LangJsFile();
        mNonExistables.clear();
        refreshView();
        return true;

    } else {

        QMessageBox msgBox;
        msgBox.setText(tr("The document has been modified."));
        msgBox.setInformativeText(tr("Do you want to save your changes?"));
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        int ret = msgBox.exec();

        switch (ret) {
          case QMessageBox::Save:
              // Save was clicked
             if(mCurrent->lastFile.get().isEmpty()) {
                 QString file = QFileDialog::getSaveFileName(this,
                                                                 tr("Select saving name"),
                                                                 tr(""),
                                                                 tr("Language files(*.js)"));
                 if(file.isEmpty() ) return false;
                 mCurrent->lastFile = file;

             }

              if(! mCurrent->save() ) {
                     QMessageBox msgBox;
                     msgBox.setWindowTitle(tr("Warning"));
                     msgBox.setIcon(QMessageBox::Warning);
                     msgBox.setText(tr("Can not save this file. Operation abored."));
                     msgBox.exec();
                     return false;
                 };

                return true;

              break;
          case QMessageBox::Discard:
              // Don't Save was clicked
              delete mCurrent;
              mCurrent = new LangJsFile();
              refreshView();
              return true;
              break;
          case QMessageBox::Cancel:
              // Cancel was clicked
            return false;
              break;
          default:
              // should never be reached
            return false;
              break;
        }


    };
}



void MainWindow::on_actionNew_triggered()
{

    safeCreateNewTrFile();
}

void MainWindow::on_actionExit_triggered()
{

    if((mCurrent == nullptr) || (mCurrent->isSaved())){
            QApplication::exit(0);
    }

    QMessageBox msgBox;
    msgBox.setText(tr("The document has been modified."));
    msgBox.setInformativeText(tr("Do you want to save your changes?"));
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    int ret = msgBox.exec();

    switch (ret) {
      case QMessageBox::Save:
        // Save was clicked
       if(mCurrent->lastFile.get().isEmpty()) {
           QString file = QFileDialog::getSaveFileName(this,
                                                           tr("Select saving name"),
                                                           tr(""),
                                                           tr("Language files(*.js)"));
           if(file.isEmpty() ) return;
           mCurrent->lastFile = file;

       }

        if(! mCurrent->save() ) {
               QMessageBox msgBox;
               msgBox.setWindowTitle(tr("Warning"));
               msgBox.setIcon(QMessageBox::Warning);
               msgBox.setText(tr("Can not save this file. Operation abored."));
               msgBox.exec();
               return;
           };

          return;
          break;
      case QMessageBox::Discard:
        QApplication::exit(0);
          return;
          break;
      case QMessageBox::Cancel:
          // Cancel was clicked
        return;
          break;
      default:
          // should never be reached
        return;
          break;
    }
}

void MainWindow::on_removeSelectedPushButton_clicked()
{
    if(mCurrent == nullptr) {
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Warning"));
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText(tr("Please open a file or create a new one."));
        msgBox.exec();
      return;
    };


    auto items = ui->allKeysListWidget->selectedItems();

    foreach(QListWidgetItem * item, items) {
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Item removal confirm"));
        msgBox.setIcon(QMessageBox::Question);
        msgBox.setText(tr("Do you want to remove key:") +" "+item->text()+"?");
        msgBox.setStandardButtons(QMessageBox::Ok| QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Ok);
        auto list = mCurrent->definitions.get();
        int ret = msgBox.exec();
        switch (ret) {
          case QMessageBox::Ok:
              list.remove(item->text());
              break;
          case QMessageBox::Cancel:
              continue;
              break;
          default:
              break;
        }
        mCurrent->definitions = list;


    };


    refreshView();

}

void MainWindow::on_renameSelectedPushButton_clicked()
{
    if(mCurrent == nullptr) {
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Warning"));
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText(tr("Please open a file or create a new one."));
        msgBox.exec();
      return;
    };


    auto items = ui->allKeysListWidget->selectedItems();

    foreach(QListWidgetItem * item, items) {

        auto list = mCurrent->definitions.get();

        bool ok;
            QString text = QInputDialog::getText(this, tr("Change key name"),
                                                 tr("Tr key name:"), QLineEdit::Normal,
                                                 item->text(), &ok);
            if (ok && !text.isEmpty()) {


                if(mCurrent->definitions.get().keys().contains(text)) {
                    QMessageBox msgBox;
                    msgBox.setWindowTitle(tr("Warning"));
                    msgBox.setIcon(QMessageBox::Warning);
                    msgBox.setText(tr("This key already exists"));
                    msgBox.exec();
                    continue;


                };


                if( list.contains( text )) {
                    QMessageBox msgBox;
                    msgBox.setWindowTitle(tr("Warning"));
                    msgBox.setIcon(QMessageBox::Warning);
                    msgBox.setText(tr("Can not rename to existing key"));
                    msgBox.exec();
                    continue;

                };

         list[text] = list[item->text()];
         list.remove(item->text());
         mCurrent->definitions = list;


    };
   };
    refreshView();

}


void MainWindow::on_actionClose_triggered()
{
    if(mCurrent == nullptr) {
        return;
    }
    if((!(mCurrent == nullptr))&& (! mCurrent->isSaved())){
    QMessageBox msgBox;
    msgBox.setText(tr("The document has been modified."));
    msgBox.setInformativeText(tr("Do you want to save your changes?"));
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    int ret = msgBox.exec();

        switch (ret) {
          case QMessageBox::Save:
            if(mCurrent->lastFile.get().isEmpty()) {
                QString file = QFileDialog::getSaveFileName(this,
                                                                tr("Select saving name"),
                                                                tr(""),
                                                                tr("Language files(*.js)"));
                if(file.isEmpty() ) return;
                mCurrent->lastFile = file;

            }

             if(! mCurrent->save() ) {
                    QMessageBox msgBox;
                    msgBox.setWindowTitle(tr("Warning"));
                    msgBox.setIcon(QMessageBox::Warning);
                    msgBox.setText(tr("Can not save this file. Operation abored."));
                    msgBox.exec();
                    return;
                };
              break;
          case QMessageBox::Discard:
              break;
          case QMessageBox::Cancel:
            return;
              break;
          default:
            return;
              break;
        }
    };

    delete mCurrent;
    mCurrent = nullptr;
    mNonExistables.clear();
    refreshView();
}



void MainWindow::on_actionOpen_triggered()
{
    if((!(mCurrent == nullptr))&& (! mCurrent->isSaved())){
    QMessageBox msgBox;
    msgBox.setText(tr("The document has been modified."));
    msgBox.setInformativeText(tr("Do you want to save your changes?"));
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    int ret = msgBox.exec();

        switch (ret) {
          case QMessageBox::Save:
            if(mCurrent->lastFile.get().isEmpty()) {
                QString file = QFileDialog::getSaveFileName(this,
                                                                tr("Select saving name"),
                                                                tr(""),
                                                                tr("Language files(*.js)"));
                if(file.isEmpty() ) return;
                mCurrent->lastFile = file;

            }

             if(! mCurrent->save() ) {
                    QMessageBox msgBox;
                    msgBox.setWindowTitle(tr("Warning"));
                    msgBox.setIcon(QMessageBox::Warning);
                    msgBox.setText(tr("Can not save this file. Operation abored."));
                    msgBox.exec();
                    return;
                };
              break;
          case QMessageBox::Discard:
              break;
          case QMessageBox::Cancel:
            return;
              break;
          default:
            return;
              break;
        }
    };


    try {
        QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open language file"),
                                                    tr(""),
                                                    tr("LangFile(*.js *.json)"));

        if(fileName.isEmpty()) return;
        mCurrent = LangJsFile::fromJsFile(fileName);

        mNonExistables.clear();
        refreshView();


    } catch(std::runtime_error err) {
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Can not open file"));
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText(tr("Can not open file: ")+err.what());
        msgBox.exec();
    }
}

void MainWindow::on_comboBoxFullName_currentTextChanged(const QString &)
{
    if(mCurrent != nullptr) {
      mCurrent->langFullName = ui->comboBoxFullName->currentText();
      mCurrent->langShortName = ui->comboBoxShortName->currentText();
    };
}

void MainWindow::on_comboBoxShortName_currentTextChanged(const QString &)
{
    if(mCurrent != nullptr) {
      mCurrent->langFullName = ui->comboBoxFullName->currentText();
      mCurrent->langShortName = ui->comboBoxShortName->currentText();
    };
}



void MainWindow::on_actionSave_triggered()
{
     if(mCurrent != nullptr) {

         if(mCurrent->lastFile.get().isEmpty()) {

         }



       mCurrent->save();
     };
}

void MainWindow::on_actionExport_to_JSON_triggered()
{
    if(mCurrent == nullptr) return;
    QString file = QFileDialog::getSaveFileName(this,
                                                    tr("Select export name"),
                                                    tr(""),
                                                    tr("Language files json(*.json)"));
    QJsonObject data = mCurrent->toJsonObject();
    QFile l_saveFile(file);
    if(! l_saveFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
        return;
    };





    QJsonDocument doc( data );

    QTextStream stream(&l_saveFile);
    stream << doc.toJson(QJsonDocument::Indented);
    l_saveFile.close();


}

void MainWindow::on_actionSave_as_triggered()
{
    if(mCurrent == nullptr) return;
    QString file = QFileDialog::getSaveFileName(this,
                                                    tr("Select saving name"),
                                                    tr(""),
                                                    tr("Language files(*.js)"));
    if(file.isEmpty() ) return;
    mCurrent->saveAs(file);
}

void MainWindow::on_findLineEdit_textChanged(const QString &)
{
    refreshView();
}

void MainWindow::on_allKeysListWidget_itemSelectionChanged()
{
    if(ui->allKeysListWidget->selectedItems().length() > 0) {
        mCurrentlySelected = ui->allKeysListWidget->selectedItems().at(0)->text();
    } else {
        mCurrentlySelected = QString();
    }
}

void MainWindow::on_saveValuesPushButton_clicked()
{
    if(! mCurrent) return;
    if(mCurrentlySelected.get().isEmpty()) return;
    auto defs = mCurrent->definitions.get();
    QStringList AllData;
    for(int i=0; i<ui->optonsListWidget->count(); i++) {
      AllData <<   ui->optonsListWidget->item(i)->text();
    };

    defs[mCurrentlySelected.get()] = AllData;
    mCurrent->definitions = defs;
}

void MainWindow::on_addTranslationPushButton_clicked()
{
    ui->optonsListWidget->addItem("CHANGE ME");
    makeItemsEditable();
}

void MainWindow::on_removeSelectedTranslation_clicked()
{
    if(ui->allKeysListWidget->selectedItems().length() > 0) {
      auto item = ui->optonsListWidget->takeItem( ui->optonsListWidget->row(ui->optonsListWidget->selectedItems().at(0)) );
      delete item;
    }
}

void MainWindow::on_actionSync_dir_triggered()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Project Directory"),
                                                "",
                                                QFileDialog::ShowDirsOnly
                                                | QFileDialog::DontResolveSymlinks);
    if(dir.isEmpty()) return;
    QStringList allFiles;
    QDirIterator it(dir, QStringList() << "*.*", QDir::Files, QDirIterator::Subdirectories);
    while (it.hasNext()) {
      allFiles.append(it.next());
    };

    generateAddition(allFiles, true);
}

void MainWindow::on_actionImport_keys_from_lang_file_triggered()
{
    if(! mCurrent) {
      if(! safeCreateNewTrFile()) return;
    };
    try {
    QString fileName = QFileDialog::getOpenFileName(this,
                                                tr("Open language file"),
                                                tr(""),
                                                tr("LangFile(*.js)"));
    if(fileName.isEmpty()) return;

    LangJsFile* data = LangJsFile::fromJsFile(fileName);
    auto defs = mCurrent->definitions.get();

    foreach(QString key, data->definitions.get().keys()) {
        if(!defs.contains(key)) {
            QStringList a;
            a<<key;
          defs[key] = a;
        };
    };
    mCurrent->definitions = defs;

    } catch(std::runtime_error err) {
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Can not open file"));
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setText(tr("Can not open file: ")+err.what());
        msgBox.exec();
    }
    refreshView();
}

void MainWindow::on_actionExtract_innerHTML_to_tr_triggered()
{
    HTMLTrImbunerDialog * dialog = new  HTMLTrImbunerDialog();
    dialog->setAttribute( Qt::WA_DeleteOnClose );
    dialog->show();
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::about(this, tr("JS-Linguist"), tr("JS-Linguist \n\n\
A simple program to work with js_tr project \n\
and automatize webpages translation\n\
Copyright (C) 2016 Vokhmin Ivan<cabalbl4@gmail.com>\n\
\n\
This program is free software: you can redistribute it and/or modify\n\
it under the terms of the GNU General Public License as published by\n\
the Free Software Foundation, either version 3 of the License, or\n\
(at your option) any later version.\n\
\n\
This program is distributed in the hope that it will be useful,\n\
but WITHOUT ANY WARRANTY; without even the implied warranty of\n\
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n\
GNU General Public License for more details.\n\
\n\
You should have received a copy of the GNU General Public License\n\
along with this program.  If not, see <http://www.gnu.org/licenses/>.\n\n\
js_tr project: https://github.com/Cabalbl4/js_tr\n\n\
this project: https://github.com/Cabalbl4/js-linguist"));

}

void MainWindow::on_actionMore_help_triggered()
{
    QString link = "https://github.com/Cabalbl4/js-linguist/wiki";
    QDesktopServices::openUrl(QUrl(link));
}




