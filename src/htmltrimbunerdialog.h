#ifndef HTMLTRIMBUNERDIALOG_H
#define HTMLTRIMBUNERDIALOG_H

#include <QWidget>
#include <QWebPage>

namespace Ui {
class HTMLTrImbunerDialog;
}

class HTMLTrImbunerDialog : public QWidget
{
    Q_OBJECT

public:
    explicit HTMLTrImbunerDialog(QWidget *parent = 0);
    ~HTMLTrImbunerDialog();

private slots:
    void on_selectHtmlFileBtn_clicked();

    void on_startPushButton_clicked();

    void on_exitPushButton_clicked();
    void onLoadFinished(bool ok);

protected:

    bool makeBackup(const QString& filename, const QString& backup);
    bool trenfuse(const QString& filename);

private:
    QWebPage mPage;
    QString mLastFile;
    Ui::HTMLTrImbunerDialog *ui;
};

#endif // HTMLTRIMBUNERDIALOG_H
