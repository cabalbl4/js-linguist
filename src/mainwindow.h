#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "parserdispatcher.h"
#include "modalprogressmeter.h"
#include "property.h"


#include "langjsfile.h"
#include <QMainWindow>

class MainWindow;
typedef property<QString, MainWindow> sproperty;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);


    void closeEvent (QCloseEvent *event) override;
    ~MainWindow();
public slots:
    void parserAddResult(QMap<QString, QList<int>> data);
    void parserSyncResult(QMap<QString, QList<int> > data);
private slots:
    void on_comboBoxFullName_currentIndexChanged(int index);

    void on_comboBoxShortName_currentIndexChanged(int index);

    void on_actionAdd_from_dir_triggered();

    void refreshView();


    void on_addTranslationKeyPushButton_clicked();

    void on_actionNew_triggered();

    void on_actionExit_triggered();

    void on_removeSelectedPushButton_clicked();

    void on_actionAdd_from_file_triggered();

    void on_actionClose_triggered();

    void on_renameSelectedPushButton_clicked();

    void on_actionOpen_triggered();

    void on_comboBoxFullName_currentTextChanged(const QString &arg1);

    void on_comboBoxShortName_currentTextChanged(const QString &arg1);

    void on_actionSave_triggered();

    void on_actionSave_as_triggered();

    void on_findLineEdit_textChanged(const QString &);

    void on_allKeysListWidget_itemSelectionChanged();

    void on_saveValuesPushButton_clicked();

    void on_addTranslationPushButton_clicked();

    void on_removeSelectedTranslation_clicked();

    void on_actionSync_dir_triggered();

    void on_actionImport_keys_from_lang_file_triggered();

    void on_actionExtract_innerHTML_to_tr_triggered();

    void on_actionAbout_triggered();

    void on_actionMore_help_triggered();

    void on_actionExport_to_JSON_triggered();

private:
    bool safeCreateNewTrFile();
    void generateAddition(const QStringList& allFiles, bool syncMode);
    void makeItemsEditable();

    Ui::MainWindow *ui;
    LangJsFile * mCurrent;
    ModalProgressMeter * mProgressDialog;

   sproperty mCurrentlySelected;
   QStringList mNonExistables;
    void selectionChangeTrigger( sproperty &param, const QString& what);
};

#endif // MAINWINDOW_H
