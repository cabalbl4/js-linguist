#ifndef PARSERDISPATCHER_H
#define PARSERDISPATCHER_H

#include <QObject>
#include <QMutex>
#include "abstractparser.h"

namespace parsers {

class ParserDispatcher : public AbstractParser
{
    Q_OBJECT    
public:
    enum PARSER_TYPE {
        PARSER_HTML,
        PARSER_JAVASCRIPT
    };

    explicit ParserDispatcher(
            QList<QString> files,
            QMap<QString, PARSER_TYPE> formatToParsing
            );
    ~ParserDispatcher(){ delete mMutex; }
public slots:
    virtual void startParsing();
    virtual void abort();
    void receiveParserProgress(AbstractParser * who, int progress);
    void receiveFatals(AbstractParser * who, QStringList fatals);
    void receiveResults(AbstractParser * who, QMap<QString, QList<int>> data);

protected:
    void merge(QMap<QString, QList<int> > data);

    QList<QString> mFiles;
    int mProgress;
    QMap<QString, PARSER_TYPE> mFormatToParsing;
    QMap<AbstractParser*, int> mProgresses;
    QMap<AbstractParser*, QThread*> mWorkers;
    QMap<QString, QList<int> > mData;
    QMutex * mMutex;




signals:
    void startAll();
    void abortAll();
    void overallProgress(int p);
    void overallErrors(QStringList err);
    void overallResult(QMap<QString, QList<int>> data);
};

}
#endif // PARSERDISPATCHER_H
