#ifndef TRANSLATIONVARIANTSEDITFORM_H
#define TRANSLATIONVARIANTSEDITFORM_H

#include <QWidget>

namespace Ui {
class translationVariantsEditForm;
}

class translationVariantsEditForm : public QWidget
{
    Q_OBJECT

public:
    explicit translationVariantsEditForm(QWidget *parent = 0);
    ~translationVariantsEditForm();

private:
    Ui::translationVariantsEditForm *ui;
};

#endif // TRANSLATIONVARIANTSEDITFORM_H
