#ifndef LANGJSFILE_H
#define LANGJSFILE_H

#include <QStringList>
#include <QMap>
#include <QFile>

#include "property.h"

class LangJsFile {
properties
public:
    static LangJsFile *fromJsFile(const QString& file);

    property <QString, LangJsFile> lastFile;
    property <QString, LangJsFile> langFullName;
    property <QString, LangJsFile> langShortName;
    property <QMap<QString, QStringList>, LangJsFile> definitions;
    bool isSaved() { return mIsSaved; }

    bool saveAs(const QString& fileName);
    bool save();
    QJsonObject toJsonObject();



    explicit LangJsFile();

private:
    inline void init_property() {
      expose_property(langFullName);
      expose_property(langShortName);
      expose_property(lastFile);
      expose_property(definitions);
      langFullName.setSetter(this, &LangJsFile::saveTrigger);
      langShortName.setSetter(this, &LangJsFile::saveTrigger);
      lastFile.setSetter(this, &LangJsFile::saveTrigger);
      definitions.setSetter(this, &LangJsFile::definitionsSaveTrigger);
      mIsSaved = true;
    }


    void saveTrigger(property<QString, LangJsFile> &param, const QString& what) {
            *(param.valuePtr()) = what;
            mIsSaved = false;
    }

    void definitionsSaveTrigger(property <QMap<QString, QStringList>,LangJsFile> &param, const QMap<QString, QStringList> &what) {
        *(param.valuePtr()) = what;
        mIsSaved = false;
    }


     bool mIsSaved;



};

#endif // LANGJSFILE_H
