#include "modalprogressmeter.h"

ModalProgressMeter::ModalProgressMeter(const QString &caption, QWidget *parent) : QDialog(parent, Qt::CustomizeWindowHint|Qt::WindowTitleHint),
        ui(new Ui::ModalProgressMeter)
{
    ui->setupUi(this);
    ui->label->setText(caption);

}

void ModalProgressMeter::progress(int val)
{
    ui->progressBar->setValue(val);
    this->repaint();

}

void ModalProgressMeter::annihilate()
{
    if( ui->textEdit->toPlainText().length()>0) return;
    this->close();
    this->deleteLater();
}

void ModalProgressMeter::recieveErrStrings(QStringList strs)
{
    foreach(QString str, strs) {
      ui->textEdit->appendHtml("<font color='red'><b>"+tr("error")+"&nbsp;</b></font>"+str+"<br>");
    };
}

