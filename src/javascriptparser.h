#ifndef JAVASCRIPTPARSER_H
#define JAVASCRIPTPARSER_H

#include <atomic>
#include <QObject>
#include <QFile>

#include "abstractparser.h"


namespace parsers {

class JavascriptParser : public AbstractParser
{
    Q_OBJECT
public:
    static  QMap<QString, QList<int>> parseJSString(const QString& data_s);

    explicit JavascriptParser(const QString &_javascriptFile);
    virtual ~JavascriptParser(){}

public slots:
    virtual void startParsing();
    virtual void abort();


protected:
    QFile mFile;
    std::atomic_bool mEnd;
    QMap<QString, QList<int>> parseNestedStrings(const QString &script);
    QString parseTR(QString script);
    int parseTRForm(QString script);
signals:

};

}

#endif // JAVASCRIPTPARSER_H
