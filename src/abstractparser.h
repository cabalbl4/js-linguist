#ifndef ABSTRACTPARSER
#define ABSTRACTPARSER

#define MAX_SIMUL_THREADS 30

#include <QObject>
#include <QMap>
#include <QList>
#include <QString>
#include <QStringList>
#include <QSemaphore>

typedef QList<int> ttype;
typedef QMap<QString, ttype> ttypemap;
Q_DECLARE_METATYPE(ttype)
Q_DECLARE_METATYPE(ttypemap)

namespace parsers {
class AbstractParser : public QObject {
    Q_OBJECT


public:
    AbstractParser() : QObject(0) {}
    virtual ~AbstractParser(){}


protected:
    static QSemaphore* mSemaphore;
public slots:
    virtual void startParsing()=0;
    virtual void abort()=0;

signals:
    void fatalErrors(AbstractParser *who, QStringList allerrors);
    void result(AbstractParser *who, ttypemap data);
    void progress(AbstractParser *who, int percent);




};






}

#endif // ABSTRACTPARSER

