#include "parserdispatcher.h"
#include <QFile>
#include <QThread>
#include <QFileInfo>
#include <QDebug>
#include <QApplication>

#include "htmlparser.h"
#include "javascriptparser.h"

namespace parsers {



ParserDispatcher::ParserDispatcher(
        QList<QString> files,
        QMap<QString, PARSER_TYPE> formatToParsing
        ) : AbstractParser(),
    mFiles(files),
    mProgress(0),
    mFormatToParsing(formatToParsing),
    mMutex(new QMutex(QMutex::Recursive))
{

}

void ParserDispatcher::startParsing()
{
    QMutexLocker l(mMutex);
    foreach(QString fileName, mFiles) {
        QFile file(fileName);
        if(! file.exists()) continue;
        QFileInfo info(file);
       if( mFormatToParsing.contains( info.completeSuffix().toUpper() ) ) {
           AbstractParser * parser = nullptr;

           qDebug()<<info.completeSuffix().toUpper() << mFormatToParsing[info.completeSuffix().toUpper()];
           switch (mFormatToParsing[info.completeSuffix().toUpper()]) {
           case PARSER_HTML:
               parser = new HTMLParser(fileName);
               mWorkers.insert(parser, nullptr);
               break;
           case PARSER_JAVASCRIPT:
               parser = new JavascriptParser(fileName);
               QThread *worker = new QThread();
               parser->moveToThread(worker);
               connect(this,SIGNAL(startAll()), worker, SLOT(start()));
                connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
               mWorkers.insert(parser, worker);
               break;
           }
           if(parser == nullptr) continue;
           QCoreApplication::processEvents();


           connect(parser, SIGNAL(progress(AbstractParser*,int))
                   , this, SLOT(receiveParserProgress(AbstractParser*,int)));

           connect(parser, &AbstractParser::result, this,
            &ParserDispatcher::receiveResults );
           connect(parser, SIGNAL(fatalErrors(AbstractParser*,QStringList)), this,
                                  SLOT(receiveFatals(AbstractParser*,QStringList)));
           connect(this, SIGNAL(abortAll()), parser, SLOT(abort()));
           connect(this,SIGNAL(startAll()), parser, SLOT(startParsing()));

           mProgresses.insert(parser, 0);







        };
    };
    if(mWorkers.keys().length() > 0) {
        emit startAll();
    } else {
        emit progress(this, 100);
        emit overallProgress(100);
        result(this, mData);
        overallResult(mData);
    };
    QCoreApplication::processEvents();

}

void ParserDispatcher::abort()
{
    QMutexLocker l(mMutex);
    emit abortAll();
    foreach(QThread * worker, mWorkers.values()) {
        if(worker){
            worker->quit();
            worker->deleteLater();
        }

    };
    mWorkers.clear();
    mProgresses.clear();
}

void ParserDispatcher::receiveParserProgress(AbstractParser *who, int prog)
{
    QMutexLocker l(mMutex);
    mProgresses[who] = prog;
    int total = 0;
    foreach(int pr, mProgresses.values()) total += pr;
    if((total/mProgresses.values().length())<= mProgress) return;
    mProgress = total/mProgresses.values().length();
    emit progress(this, total/mProgresses.values().length());
    emit overallProgress(total/mProgresses.values().length());
    QApplication::processEvents();

}

void ParserDispatcher::receiveFatals(AbstractParser *who, QStringList fatals)
{
    QMutexLocker l(mMutex);
    mProgresses[who] = 100;
    int total = 0;
    foreach(int pr, mProgresses.values()) total += pr;
    emit progress(this, total/mProgresses.values().length());
    emit overallProgress(total/mProgresses.values().length());
    emit fatalErrors(this, fatals);
    emit overallErrors(fatals);
    if(mWorkers.contains(who)) {
     if( mWorkers[who] != nullptr){
         mWorkers[who]->quit();
       //  mWorkers[who]->deleteLater();
     }
      mWorkers.remove(who);
    };
    qDebug() << "Workers remain (this one has errors)" << mWorkers.keys().length();
    if(mWorkers.keys().length() == 0) {
      result(this, mData);
      overallResult(mData);
      this->deleteLater();
    };
    QCoreApplication::processEvents();
}

void ParserDispatcher::receiveResults(AbstractParser *who, QMap<QString, QList<int> > data)
{

    qDebug()<<"results of"<<(ptrdiff_t)who;
    QMutexLocker l(mMutex);
    if(mWorkers.contains(who)) {
        if( mWorkers[who] != nullptr){
            mWorkers[who]->quit();
         //   mWorkers[who]->deleteLater();
        }
      mWorkers.remove(who);
    } else {
         qDebug()<<"NOT FOUND!!!"<<(ptrdiff_t)who;
    }
    merge(data);

        qDebug() << "Workers remain " << mWorkers.keys().length();




    if(mWorkers.keys().length() == 0) {
      result(this, mData);
      overallResult(mData);
      this->deleteLater();
    };

QCoreApplication::processEvents();

}

void ParserDispatcher::merge(QMap<QString, QList<int> > data)
{
    foreach(QString key, data.keys()) {
        if(mData.contains(key)) {
           QList<int> what = mData[key];
           foreach(int val,data[key]) {
               if(what.count(val)==0) what.append(val);
           }
           mData[key]=what;
        } else {
           mData[key] = data[key];
        }
    }
}

}
