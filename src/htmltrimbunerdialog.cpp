#include "htmltrimbunerdialog.h"
#include "ui_htmltrimbunerdialog.h"

#include <QWebPage>
#include <QWebFrame>
#include <QWebElement>
#include <QWebElementCollection>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>

#include <QFileDialog>

HTMLTrImbunerDialog::HTMLTrImbunerDialog(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HTMLTrImbunerDialog)
{
    ui->setupUi(this);
}

HTMLTrImbunerDialog::~HTMLTrImbunerDialog()
{
    delete ui;
}

void HTMLTrImbunerDialog::on_selectHtmlFileBtn_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                tr("Select file to work with"),
                                                tr(""),
                                                tr("HTML files(*.html *.htm)"));
    if(fileName.isEmpty()) return;
    ui->htmlFileLineEdit->setText(fileName);
    ui->htmlBackupLineEdit->setText(fileName+".bak");
}

void HTMLTrImbunerDialog::on_startPushButton_clicked()
{

    if(ui->backupCheckBox->isChecked()) {
        if(! makeBackup(
                    ui->htmlFileLineEdit->text(),
                    ui->htmlBackupLineEdit->text()
                    )) {

            QMessageBox msgBox;
            msgBox.setWindowTitle(tr("Warning"));
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setText(tr("Can not make backup. Aborted."));
            msgBox.exec();
            return;
        }
    }


    if(! trenfuse(ui->htmlFileLineEdit->text())) {
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Warning"));
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText(tr("Error in file processing. Can not read. Main file should be safe."));
        msgBox.exec();

        return;
    }


}

bool HTMLTrImbunerDialog::makeBackup(const QString &filename, const QString &backup)
{
    if (QFile::exists(backup))
    {
        QFile::remove(backup);
    }

    return QFile::copy(filename, backup);
}

bool HTMLTrImbunerDialog::trenfuse(const QString &filename)
{

    mLastFile = filename;
    this->setDisabled(true);
    QFile file(filename);

    if(! file.open(QIODevice::ReadOnly | QIODevice::Text)) {
         this->setDisabled(false);
        return false;
    };







   QWebFrame *frame = mPage.mainFrame();
   connect(frame, SIGNAL(loadFinished(bool)), this, SLOT(onLoadFinished(bool)));
   frame->setHtml(QString(file.readAll()));
   file.close();
   return true;

}

void HTMLTrImbunerDialog::on_exitPushButton_clicked()
{
    this->close();

}

void HTMLTrImbunerDialog::onLoadFinished(bool)
{
    QFile file(mLastFile);
     this->setDisabled(false);
     QWebFrame *frame = mPage.mainFrame();

    QWebElementCollection col = frame->findAllElements("*");


    foreach(QWebElement el, col) {

       if(el.attributeNames().indexOf("tr") != -1) {
         continue;
       };


       if(el.tagName().toLower() == "script") continue;
       if(el.tagName().toLower() == "style") continue;

       if(el.findAll("*").count() == 0) {
           if(el.toPlainText().isEmpty()) continue;
         qDebug() << el.tagName() <<"adding to tr:" <<el.toPlainText();
         el.setAttribute("tr", el.toPlainText());
       };



    };

    if(! file.open(QIODevice::WriteOnly | QIODevice::Text)) {

        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Warning"));
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText(tr("Error in file processing. Can not write. Main file should be safe."));
        msgBox.exec();
        return;
    }

    QTextStream stream(&file);

     stream << frame->toHtml();

     file.close();

     QMessageBox msgBox;
     msgBox.setWindowTitle(tr("Ok"));
     msgBox.setIcon(QMessageBox::Information);
     msgBox.setText(tr("Done"));
     msgBox.exec();
     this->close();

}
