#ifndef MODALPROGRESSMETER_H
#define MODALPROGRESSMETER_H

#include <QWidget>
#include <QDialog>
#include "ui_modalprogressmeter.h"

class ModalProgressMeter : public QDialog
{
    Q_OBJECT
public:
    explicit ModalProgressMeter(const QString& caption, QWidget *parent = 0);
    ~ModalProgressMeter() { delete ui; }
signals:

public slots:
    void progress(int val);
    void annihilate();
    void recieveErrStrings(QStringList strs);
private:
    Ui::ModalProgressMeter *ui;
};

#endif // MODALPROGRESSMETER_H
