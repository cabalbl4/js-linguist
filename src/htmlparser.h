#ifndef HTMLPARSER_H
#define HTMLPARSER_H

#include <atomic>
#include <QObject>
#include <QFile>
#include <QXmlDefaultHandler>
#include <QDomNode>
#include <QWebPage>

#include <abstractparser.h>
namespace parsers {



class HTMLParser : public AbstractParser
{
    Q_OBJECT

public:
    HTMLParser(const QString &_javascriptFile);
    virtual ~HTMLParser(){}

public slots:
    virtual void startParsing();
    virtual void abort();
    virtual void afterLoad(bool ok);

private:
    QMap<QString,QList<int>> traverse(const QDomNode& node);
      QWebPage mPage;

protected:
    QFile mFile;
    std::atomic_bool mEnd;

};

}

#endif // HTMLPARSER_H
