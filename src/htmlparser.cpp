#include "htmlparser.h"


#include <QThread>
#include <QDebug>
#include <QApplication>
#include <QWebFrame>
#include <QWebElementCollection>
#include <QWebPage>
#include <QStringList>

#include "javascriptparser.h"

namespace parsers {



HTMLParser::HTMLParser(const QString &_javascriptFile)
{
    mFile.setFileName(_javascriptFile);
    mEnd = false;
}

void HTMLParser::startParsing()
{

mSemaphore->acquire();
  QStringList fatals;
  if(! mFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
    QString err = tr("Can not open file:")+mFile.fileName();
    fatals.append(err);
    emit(fatalErrors(this, fatals));
    this->deleteLater();
    mSemaphore->release();
    return;
  };

 emit progress(this, 0);




emit progress(this, 50);

  QWebFrame *frame = mPage.mainFrame();
  connect(frame, SIGNAL(loadFinished(bool)), this, SLOT(afterLoad(bool)));
  frame->setHtml(QString(mFile.readAll()));
  mFile.close();




}

void HTMLParser::abort()
{
    mEnd = true;
    this->deleteLater();
    blockSignals(this);
}

void HTMLParser::afterLoad(bool )
{
        QMap<QString,QList<int>> l_result;
    QWebFrame *frame = mPage.mainFrame();
    QWebElementCollection col = frame->findAllElements("*");

    foreach(QWebElement el, col) {


      QString trStr = "";
      int trForm = 1;
      QString trAttr;
      QStringList trAttrPos = { "tr", "data-tr"};
      foreach (QString posAttr, trAttrPos) {
          if(el.attributeNames().indexOf(posAttr) != -1) {
              trAttr = posAttr;
              break;
          }
      }

       if(( ! trAttr.isEmpty() ) && el.attributeNames().indexOf(trAttr) != -1) {
         trStr = el.attribute(trAttr);
         if(trStr.isEmpty()) {
             trStr = el.toInnerXml();
         }
       };

       if(el.tagName() == "script") {
         QMap<QString,QList<int>> l_scr_result = JavascriptParser::parseJSString( el.toInnerXml() );
         foreach(QString key, l_scr_result.keys()) {
           QList<int> ikeys = l_result[key];
           foreach(int f, l_scr_result[key]) {
             if(ikeys.indexOf(f)==-1) ikeys.append(f);
           };
           l_result[key] = ikeys;

         };


       };

       QStringList trFormList = { "trform", "data-trform" };
       QString trFromAtr;
       foreach (QString trForm, trFormList) {
           if(el.attributeNames().indexOf(trForm) != -1) {
               trFromAtr = trForm;
               break;
           }
       }

       if((! trFromAtr.isEmpty() ) && el.attributeNames().indexOf(trFromAtr) != -1) {
         QString trFormStr = el.attribute(trFromAtr);
         bool ok =  true;
         int test = trFormStr.toInt(&ok);
         if(ok) trForm = test;
       };
       if(!trStr.isEmpty()) {
           QList<int> tlist = l_result[trStr];
           if(tlist.count(trForm) == 0) tlist.append(trForm);
           l_result[trStr] = tlist;
       }




       //Atr support
       QStringList atrList = { "atr", "data-atr" };
       QString trAtr;

       foreach (QString trForm, atrList) {
           if(el.attributeNames().indexOf(trForm) != -1) {
               trAtr = trForm;
               break;
           }
       }
       if(! trAtr.isEmpty()) {
            if(el.attributeNames().indexOf(trAtr) != -1) {
             QString attrList = el.attribute(trAtr);
              if(! attrList.isEmpty()) {
                  attrList.replace(" ", "");
                  QStringList list = attrList.split(",");
                  for(int i=0; i<list.length(); i++) {
                      QString attr = list.at(i);
                      if( attr.isEmpty() ) continue;
                      if( attr.isNull() ) continue;
                      QString trStr = el.attribute(attr);
                      if(trStr.isEmpty()) continue;
                      if(l_result.contains(trStr)) continue;
                      QList<int> tlist;
                      tlist.append(1);
                      l_result[trStr] = tlist;
                  };
              };
            };
       };
    };

  mSemaphore->release();
    if(mEnd == true) return;
   // qDebug() << "HTML parser all good";
    emit progress(this, 100);
    emit(result(this, l_result));
    this->deleteLater();
}




QMap<QString, QList<int> > HTMLParser::traverse(const QDomNode &node)
{
    QMap<QString, QList<int> > nodeAttrs;
    QString what = ""; int form = 1;
    for(int i=0; i< node.attributes().length(); i++) {
        QDomNode at = node.attributes().item(i);
       // if(! (at.toAttr() == 0)) continue;
        if(at.toAttr().name() == "tr") {
          what =   at.toAttr().value();
        };
        if(at.toAttr().name() == "trform") {
          bool ok = true;
          int test = at.toAttr().value().toInt(&ok);
          if(ok) form = test;
        };
        if(! what.isEmpty()) {
          QList<int> l;
          l << form;
          nodeAttrs[what] = l;
        };
    }


    for(int i=0;i< node.childNodes().length(); i++) {
        QDomNode c_node = node.childNodes().at(i);
        QMap<QString, QList<int> >l_result = traverse(c_node);
        foreach(QString key, l_result.keys()) {
          if(nodeAttrs.contains(key)) {
              QList<int> adds = nodeAttrs[key];
              foreach(int i, l_result[key]) {
                  if(adds.contains(i)) continue;
                  adds.append(i);
              };
              nodeAttrs[key] = adds;

          }  else {
              nodeAttrs[key] = l_result[key];
          }
        };


    }
    return nodeAttrs;
}

}

