#include "mainwindow.h"
#include "abstractparser.h"
#include <QApplication>

QSemaphore* parsers::AbstractParser::mSemaphore = new QSemaphore(MAX_SIMUL_THREADS);

int main(int argc, char *argv[])
{
    qRegisterMetaType<ttype>("ttype");
    qRegisterMetaType<ttypemap>("ttypemap");
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
