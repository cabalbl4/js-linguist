#include "langjsfile.h"

#include <QByteArray>
#include <QJsonArray>
#include <QScriptEngine>
#include <QScriptValueIterator>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QTextStream>
#include <QDateTime>
#include <QFileInfo>

LangJsFile* LangJsFile::fromJsFile(const QString& file)
{
    QFile jsFile(file);
    if(! jsFile.open(QIODevice::ReadOnly | QIODevice::Text)) throw std::runtime_error("Can not open file");
    QByteArray all = jsFile.readAll();
    jsFile.close();
    QFileInfo info(jsFile);
    if( info.suffix().toUpper() == "JS" ) {

            QScriptEngine e;
            e.evaluate(QString(all));
            QScriptValue object = e.globalObject().property("_i18n_lang");
            QScriptValueIterator it(object);
            QString l_LangName = "";
            LangJsFile* l_result = new LangJsFile();

            l_result->lastFile = file;

            while (it.hasNext()) {
                it.next();
                l_LangName = it.name();
            };

            l_result->langShortName = l_LangName;
            l_result->langFullName = e.globalObject().property("_i18n_lang").property(l_LangName).property("desc").property("fullname").toString();


            //Parse
            QScriptValue langObj = e.globalObject().property("_i18n_lang").property(l_LangName);
            QScriptValue langData = langObj.property("data");
            QScriptValueIterator ito(langData);

            auto defs = l_result-> definitions.get();
            while(ito.hasNext()) {
              ito.next();
              QStringList variants;
              int length = langData.property(ito.name()).property("length").toInt32();
              for(int i=0; i<length; i++) {
                  variants << langData.property(ito.name()).property(i).toString();
              };
             defs[ito.name()] = variants;
            };
            l_result->definitions = defs;
            l_result->mIsSaved = true;

            return l_result;

    } else if (info.suffix().toUpper() == "JSON") {
        QJsonDocument doc = QJsonDocument::fromJson(all);
        QJsonObject obj = doc.object();
        LangJsFile* l_result = new LangJsFile();
        l_result->langFullName = obj["desc"].toObject()["fullname"].toString();
        l_result->langShortName = obj["desc"].toObject()["shortname"].toString();

        QJsonObject data = obj["data"].toObject();
        QStringList keys = data.keys();
         auto defs = l_result-> definitions.get();
        foreach(QString key , keys) {
            QJsonArray arr = data[key].toArray();
            QStringList vararr;
            for(int i=0; i<arr.size(); i++) {
                vararr << arr.at(i).toString();
            }
            defs[key] = vararr;

        };

        l_result->definitions = defs;
        l_result->mIsSaved = true;

        return l_result;


    } else {
         return nullptr;
    }
}

bool LangJsFile::saveAs(const QString &fileName)
{
    this->lastFile = fileName;
    return save();
}

bool LangJsFile::save()
{
   if(this->lastFile.get().isEmpty()) {
       return false;
   };

   QFile l_saveFile(lastFile.get());
   if(! l_saveFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
       return false;
   };

    if(langShortName.get().isEmpty()) langShortName = QString("noname");
   QString ident = "var global = this;\n if(typeof global._i18n_lang == 'undefined')  global._i18n_lang = {}; \n\n";
   QString langString = "global._i18n_lang." +this->langShortName.get() +" =";



   QJsonDocument doc( toJsonObject() );

   QTextStream stream(&l_saveFile);
   stream << ident;
   stream << langString;
   stream << doc.toJson(QJsonDocument::Indented);



   l_saveFile.close();
   mIsSaved = true;
   return true;

}

QJsonObject LangJsFile::toJsonObject()
{
    QJsonObject obj;
    QJsonObject data;
    QJsonObject desc;

    if(langFullName.get().isEmpty()) langFullName = QString("noname");
    desc["fullname"] = langFullName.get();
    desc["shortname"] = langShortName.get();
    QDateTime thisTime = QDateTime::currentDateTime();
    desc["last-modify"] = thisTime.toString("dd.MM.yyyy hh:mm:ss");
    obj["desc"] = desc;
    foreach(QString name, definitions.get().keys()) {
      data[name] = QJsonArray::fromStringList( definitions.get()[name] );
    };
    obj["data"] = data;
    return obj;
}

LangJsFile::LangJsFile()
{
    init_property();
}

