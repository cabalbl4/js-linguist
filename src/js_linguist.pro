#-------------------------------------------------
#
# Project created by QtCreator 2016-06-08T21:27:00
#
#-------------------------------------------------

QT       += core gui script xml webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = js_linguist
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    langjsfile.cpp \
    javascriptparser.cpp \
    htmlparser.cpp \
    parserdispatcher.cpp \
    modalprogressmeter.cpp \
    translationvariantseditform.cpp \
    htmltrimbunerdialog.cpp

HEADERS  += mainwindow.h \
    langjsfile.h \
    property.h \
    abstractparser.h \
    javascriptparser.h \
    htmlparser.h \
    parserdispatcher.h \
    modalprogressmeter.h \
    translationvariantseditform.h \
    htmltrimbunerdialog.h

FORMS    += mainwindow.ui \
    modalprogressmeter.ui \
    translationvariantseditform.ui \
    htmltrimbunerdialog.ui

RESOURCES += \
    langs.qrc
CONFIG += c++11
