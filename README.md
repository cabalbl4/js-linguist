# js-linguist
A tool to generate language files based on HTML/JS usage data for js_tr project: https://github.com/Cabalbl4/js_tr

Can synchronize projects, extract translations from generic HTML files and more.

![alt tag](http://1hub.ru/uploads/images/00/30/33/2016/06/14/762817.png)

This tool is now in release stage. All functions are available and JS/HTML parsers are working. 

### Main features
* HTML / JS parsing to get translation data
* Dir scan mode to scan or synchronize whole project
* Automatical modification of HTML files to support translation
* Easy way to manage translations

Read wiki for main controls usage https://github.com/Cabalbl4/js-linguist/wiki

### How to get
Ubuntu 14.04+ and Windows distributives available in dists folder of this repo.
<hr>
#### Windows

<a href="https://raw.githubusercontent.com/Cabalbl4/js-linguist/master/dists/windows/linguist-win32-distrib.zip">Download win32 version (zip)</a>
<hr>
#### Ubuntu
<a href="https://raw.githubusercontent.com/Cabalbl4/js-linguist/master/dists/ubuntu/js_linguist">Download Ubuntu binary</a>

<a href="https://raw.githubusercontent.com/Cabalbl4/js-linguist/master/dists/ubuntu/install.sh">Download Ubuntu deps install script</a>

### Build notice
If building, use Qt version < 5.6, as WebKit (which is used in this project) is abandoned since this Qt release (qmake will fail to find webkitwidgets). As soon as the Qt WebEngine will be stable enough this project will abandon WebKit too.


## JS_TR project 

A tool to do client-side localization in HTML and JavaScript 
https://github.com/Cabalbl4/js_tr


